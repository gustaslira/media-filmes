print('-'*30)
print('Medidor de Qualidade de Filmes - nota (Rotten Tomatoes + IMDB + Metascore)')
print('-'*30)

mv = str(input('Digite o nome do que deseja analisar: '))
qtd = int(input('Digite a quantidade de filmes que deseja analisar: '))
rotc = list()
rotp = list()
imdb = list()
meta = list()
c = 0
p = 0
i = 0
m = 0

print('*'*30)

for cont in range(0, qtd):
    rotc.append(int(input(f'Digite a nota da crítica do Rotten Tomatoes para o {cont + 1}° filme: ')))
    rotp.append(int(input(f'Digite a nota do público do Rotten Tomatoes para o {cont + 1}° filme: ')))
    imdb.append(int(input(f'Digite a nota do IMDB (sem vírgula) para o {cont + 1}° filme: ')))
    meta.append(int(input(f'Digite o Metascore para o {cont + 1}° filme: ')))

a = sum(rotc + rotp + imdb + meta)
b = qtd * 4

media = a / b

print('A pontuação de ' + mv + ' é igual a ' + str(media) + '.')



